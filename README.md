Smile v2 Development Environment Configuration
We are using Vagrant for development environments. The development environment for Smile will live in a Vagrant virtual machine, while the development and testing of the site can be done on the host machine.
Requirements
vagrant git
Clone git repository on host machine in directory of your choice
git clone https://bitbucket.org/smileconsortium/smile_v2 cd smile_v2 git pull origin develop
All development should take place on the develop branch
git checkout develop
Vagrant
Download Vagrant
Configure vagrant box:
#!bash
vagrant box add ArchLinux http://vagrant.srijn.net/archlinux-x64-2013-08-17.box
cd [smile_v2 directory]
vagrant init ArchLinux
vagrant up

vagrant ssh #to ssh into Vagrant box
# Run provisioning script
/vagrant/vagrant/provision.sh

exit #to return to host machine
Environment notes
smile_v2/vagrant is accessible at /vagrant from within the vagrant virtual machine.
You can use systemctl to control the servers installed on the vagrant virtual machine. systemctl uses the following syntax: sudo systemctl [start/stop] [service name]
There are three servers: 1. CouchDB, with service name 'couchdb'
systemctl start couchdb
The Node JS backend, with service name 'smile_backend'
systemctl start smile_backend
An NGINX reverse-proxy server, with service name 'nginx'
systemctl start nginx
The virtual machine (VM)'s port :80 is mapped to the host's port :8080.
CouchDB is available at port :5984 on the VM and at :80/couchdb on the host
The Smile v2 nodejs backend REST API is available at localhost:1337 on the VM and at localhost:8080/smile_backend on the host
To view the front-end: localhost:8080/frontend/src
To develop on the front-end: On the host machine, edit the smile_v2/frontend/src contents On the vagrant machine, make sure to run compass watch /vagrant/vagrant/frontend/src & to keep Compass running. * Compass uses SASS to automatically build the frontend/src/css/smilem.css based on the contents of frontend/src/sass/smilem.scss. Compass also automatically builds sprite sheets as defined in frontend/src/css/smilem.css