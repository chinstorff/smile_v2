/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */
 
$.using(
	
	'system.array',
	'system.object',
	'system.crypto',
	
	'application.modules.schema.user'
	
);

/**
 * Sanitizes the user document for display.
 *
 * @param object document
 *	The document to be sanitized.
 *
 * @return object
 *	The sanitized document.
 */
function sanitize_user_document(document) {
	delete document.password;

	// Define the photo URL
	if(document._attachments && document._attachments['photo']) {
		document.photoUrl = this.couchDB.baseUrl + '/' + document._id + '/photo';
	}
	
	return this.sanitize(document);

};

/**
 * Returns an error status, code and message based on the given
 * validation report.
 *
 * @param object report
 *	The validation report.
 *
 * @return mixed[]
 *	An array containing the response status, code and message.
 */
function build_validation_report(report) {
	
	// Build the error response
	var validators = report.error.byValidators;
	var code, message;
	
	if(validators.required) {
		
		code = 1;
		message = 'Missing required fields in request: ["' + validators.required.join('", "') + '"]';
		
	} else if(validators.unique) {
		
		if(validators.unique.indexOf('username') > -1) {
		
			code = 4;
			message = 'Username already exists.';
			
		} else {
		
			code = 5;
			message = 'Email address already exists.';
			
		}
		
	} else {
	
		code = 2;
		message = 'Input validation error: ["' +
			report.error.attributes.join('", "') + '"]';
			
	}
	
	return [ 400, code, message ];
}

/**
 * Sends an error response according to the error report.
 *
 * @param object report
 *	The validation error report to build the response from.
 */
function send_validation_report(report) {

	// Send the error response
	var report = build_validation_report.call(this, report);
	this.response.sendError(report[0], report[1], report[2]);
	
}


/**
 * Handles requests for user documents, according to the following requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		user: object,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 */
$.app.handle('GET', '/user/getByUsername/%s', ['smile-session'], function(username) {

	// Find the user object
	this.couchDB.get('user', 'by_username', { key: username }, this, function(records) {
	
		// Make sure the first record is here
		var record = records[0];
		
		if(!record) {
			this.response.sendError(404, 14, 'No user found with username: ' + username);
			return;
		}
		
		// Build the user object according to the request
		var user = sanitize_user_document.call(this, record);
		
		// Send the response
		this.response.send(200, {
			success: true,
			user: user
		});
	
	});

});

/**
 * Handles username availability check requests, according to the following 
 * requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		unique: boolean,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 *
 */
$.app.handle('GET', '/user/usernameIsUnique', function() {
		
	// Get the specified username
	var username = this.request.query.username;
	
	if(!username) {
		this.response.sendError(400, 1, 'Required query string parameters missing: ["username"]');
		return;
	}
	
	// Get the current user id
	var me = this.session.data.id_user;
	
	// Find users matching this username
	this.couchDB.get('user', 'by_username', { key: username }, this, function(records) {
	
		// Determine whether or not this username is unique
		var unique = !records[0];
	
		this.response.send(200, {
			success: true,
			unique: unique
		});		
	
	});
	
});

/**
 * Handles email availability check requests, according to the following 
 * requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		unique: boolean,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 *
 */
$.app.handle('GET', '/user/emailIsUnique', function() {
		
	// Get the specified username
	var email = this.request.query.email;
	
	if(!email) {
		this.response.sendError(400, 1, 'Required query string parameters missing: ["email"]');
		return;
	}
		
	// Get the current user id
	var me = this.session.data.id_user;
	
	// Find users matching this username
	this.couchDB.get('user', 'by_emailAddresses', { key: email }, this, function(records) {
	
		// Determine whether or not this username is unique
		var unique = !records[0];
	
		this.response.send(200, {
			success: true,
			unique: unique
		});		
	
	});
	
});

/**
 * Handles requests for user documents, according to the following requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		user: object,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 */
$.app.handle('POST', '/user/getUsernamesFromUUID', ['smile-session', 'smile-input'], function() {

	// Validate the input data
	var input = this.request.body;
	var uuids = input.UUIDs;
	
	if(!(uuids instanceof Array)) {
		this.response.sendError(400, 1, 'Missing required fields in request: ["UUIDs"]');
		return;
	}
	
	// Get the requested users by id
	this.couchDB.get('user', 'by_id', { keys: uuids }, this, function(records) {
	
		if (records.length < uuids.length) {
		
			// Find the missing uuids
			var available = [];
			
			for (var i in records) {
				available.push(records[i]._id);
			}
			
			var missing = $.array.diff(uuids, available);
			this.response.sendError(400, 9, 
				'User(s) not found with UUID(s): ["'  + missing.join('", "') + '"]'
			);
			
			return;
		
		}
		
		var results = [];
		
		// Build the results array
		for(var i in records) {
			results.push({
				UUID: records[i]._id,
				username: records[i].username
			});
		}
		
		this.response.send(200, {
			success: true,
			users: results
		});
	
	});
	

});

/**
 * This function is used by "POST /user/batchAdd" handler to create multiple
 * users.
 *
 * @param int index
 *	The index of this user document.
 *
 * @param object input
 *	The input data for the user being created.
 *
 * @param function(errors, id) callback
 *	The event completion callback.
 */
function run_post_user(index, input, callback) {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Validate the user input data
	$.schemaUser.validator.validate('POST', input, 
			this, function(report, document) {
		
		// Report any validation errors
		if(!report.success) {
			callback.call(this, index, input, build_validation_report.call(this, report, document));
			return;
		}
				
		// Admin authorization required to create an admin user.
		if(document.admin && !session.admin) {
	
			callback.call(this, input, [ 401, 3, 
				'Admin authorization required to create an admin user.'
			]);
		
			return;
		
		}
		
		// Build the document for CouchDB
		document = $.object.extend({}, $.schemaUser.template, document);
		
		// Restricted fields and password hash
		var now = new Date();
		document.creator = me;
		document.password = $.crypto.sha1(document.password);
		document.createdOn = [
			now.getFullYear(),
			now.getMonth() + 1,
			now.getDate(),
			now.getHours(),
			now.getMinutes(),
			now.getSeconds()
		];
		
		// Store the document in CouchDB
		this.couchDB.store(document, this, function(success, id, revision) {
	
			if(success) {

				console.log(index, input, id, revision);
				callback.call(this, index, input, false, id, revision);
				
			} else {
			
				$.error('UserModule: failed to store document in couchdb.');
				callback.call(this, input, [500, -1, 'Internal server error.']);
			
			}
		
		});
			
	});
	
}

/**
 * Handles user bulk creation requests, according to the following requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *	2. Input data is required.
 *
 * Input validators, for each user:
 *	1. Field "username" is required.
 *	2. Field "password" is required.
 *	3. Field "firstName" is required.
 *	4. Field "lastName" is required.
 *	5. Field "emailAddresses" is required.
 *	6. Current user must be an admin in order to set "admin" to TRUE.
 *	7. Values for "username" and "emailAddresses" are unique.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		UUIDs: string[]
 *	}
 *
 */
$.app.handle('POST', '/user/batchAdd', ['smile-input'], function() {

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;

	// Go through all given users
	var users = this.request.body.users;
	
	if(!(users instanceof Array)) {
		this.response.sendError(400, 2, 'Input validation error: ["users"]');
		return;
	}
	
	// Error reports and saved uuids
	var errors = [];
	var uuids = [];
	var attachments = [];
	var count = users.length;
	var processed = 0;
	var emitted = -1;
	
	// Handles the user post completion
	var handle_post_user_completion;
	handle_post_user_completion = function(index, input, error, uuid, revision) {

		$.debug('UserModule: Async user batch creation complete.');
		$.debug('UserModule: index: "' + index + '"');
		$.debug('UserModule: success: "' + (error ? 'false' : 'true')+ '"');
	
		// Register the UUID or error report
		if(error) {
		
			errors.push({
				
				input: input,
				status: error[0],
				code: error[1],
				msg: error[2]	
				
			});
			
		} else {
			
			console.log(index, error, uuid, revision);
			uuids.push(uuid);
			
			// Push the user photo attachment, if any
			var files = this.request.files;
			
			if(files['users'] && files['users'][index]) {
			
				var photo = files['users'][index]['photo'];
				
				if(photo) {
					attachments.push([ uuid, revision, photo ]);
				}
			
			}
			
		}
		
		// Detect when the task completes
		if(++processed === count) {
			
			this.response.send(200, {
				success: (errors.length < 1 && count > 0),
				UUIDs: uuids,
				errors: errors
			});
			
			// Add all attachments
			for(var i in attachments) {
				var attachment = attachments[i];				
				this.setAttachment(attachment[0], attachment[1], attachment[2], 'photo');
			}
			
		} else {
			run_post_user.call(this, ++emitted, users[emitted], handle_post_user_completion);	
		}
	
	};
	
	// Run the user post procedure, recursively
	run_post_user.call(this, ++emitted, users[emitted], handle_post_user_completion);

});

/**
 * Handles user creation requests, according to the following requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *	2. Input data is required.
 *
 * Input validators:
 *	1. Field "username" is required.
 *	2. Field "password" is required.
 *	3. Field "firstName" is required.
 *	4. Field "lastName" is required.
 *	5. Field "emailAddresses" is required.
 *	6. Current user must be an admin in order to set "admin" to TRUE.
 *	7. Values for "username" and "emailAddresses" are unique.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		UUID: string,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 *
 */
$.app.handle('POST', '/user', ['smile-input'], function() {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Get the input data
	var input = this.request.body;
	
	// Validate the user input data
	$.schemaUser.validator.validate('POST', input, 
			this, function(report, document) {
		
		// Report any validation errors
		if(!report.success) {
			send_validation_report.call(this, report, document);
			return;
			
		}
				
		// Admin authorization required to create an admin user.
		if(document.admin && !session.admin) {
	
			this.response.sendError(401, 3, 
				'Admin authorization required to create an admin user.'
			);
		
			return;
		
		}
		
		// Build the document for CouchDB
		document = $.object.extend({}, $.schemaUser.template, document);
		
		// Restricted fields and password hash
		var now = new Date();
		document.creator = me;
		document.password = $.crypto.sha1(document.password);
		document.createdOn = [
			now.getFullYear(),
			now.getMonth() + 1,
			now.getDate(),
			now.getHours(),
			now.getMinutes(),
			now.getSeconds()
		];
		
		// Store the document in CouchDB
		this.couchDB.store(document, this, function(success, id, revision) {
		
			if(success) {
			
				this.response.sendUUID(id);
					
				// Add the user photo as an attachment of this document
				var photo = this.request.files['photo'];
				
				if(photo) {
					this.setAttachment(id, revision, photo, 'photo');
				}
				
			} else {
			
				$.error('UserModule: failed to store document in couchdb.');
				this.response.sendError(500, -1, 'Internal server error.');
			
			}
		
		});
			
	});

});

/**
 * Handles user update requests, according to the following requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *	2. Input data is required.
 *	3. The user must be an admin in order to update a different user.
 *
 * Input validators:
 *	1. Field "username" is required.
 *	2. Field "password" is required.
 *	3. Field "firstName" is required.
 *	4. Field "lastName" is required.
 *	5. Field "emailAddresses" is required.
 *	6. Current user must be an admin in order to set "admin" to TRUE.
 *	7. Values for "username" and "emailAddresses" are unique.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		UUID: string,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 *
 */
$.app.handle('PUT', '/user/%w', ['smile-session', 'smile-input'], function(uuid) {

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;
	
	// The user must be an admin in order to update a different user.
	if(!session.admin && uuid !== me) {
		this.response.sendError(401, 6, 
			'Admin authorization is required to modify another user\'s details.'
		);
		
		return;
	}
	
	// Find the given user
	this.find('user', uuid, this, function(record) {
	
		// Get the input data
		var input = this.filter(this.request.body);
		input._id = record._id;
	
		// Validate the user input data
		$.schemaUser.validator.validate('PUT', input, 
				this, function(report, document) {
			
			// Report any validation errors
			if(!report.success) {
				send_validation_report.call(this, report, document);
				return;
			
			}
				
			// Admin authorization required to create an admin user.
			if(document.admin && !session.admin) {
	
				this.response.sendError(401, 3, 
					'Admin authorization required to create an admin user.'
				);
		
				return;
		
			}

            // Restricted fields and password hash
            if(document.password) {
                document.password = $.crypto.sha1(document.password);
            }

			// Build the document for CouchDB
			document = $.object.extend({}, $.schemaUser.template, record, document);

		
			// Store the document in CouchDB
			this.couchDB.store(document, this, function(success, id, revision) {
		
				if(success) {
			
					this.response.sendUUID(id);
					
					// Add the user photo as an attachment of this document
					var photo = this.request.files['photo'];
					
					if(photo) {
						this.setAttachment(id, revision, photo, 'photo');
					}
				
				} else {
			
					$.error('UserModule: failed to store document in couchdb.');
					this.response.sendError(500, -1, 'Internal server error.');
			
				}
		
			});
			
		});
	
	});

});

/**
 * Handles delete requests, according to the following requirements:
 *
 * Request validators:
 *	1. The current user must be authenticated.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 */
$.app.handle('DELETE', '/user/%w', ['smile-session'], function(uuid) {

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;
	
	// The user must be an admin in order to update a different user.
	if(!session.admin && uuid !== me) {
		this.response.sendError(401, 7, 
			'Admin authorization is required to delete another user\'s details.'
		);
		
		return;
	}
	
	// Find the user account
	this.find('user', uuid, this, function(record) {
	
		this.couchDB.delete(record, this, function(success) {
		
			if(success) {
			
				// Terminate the user session
				if(me === uuid) {
					session.authenticated = false;
					delete session.id_user;
					delete session.user;
					delete session.admin;
					delete session.time_authenticated;
				}
				
				// Send the OK response
				this.response.sendOK();
				
			} else {
				this.response.sendError(500, -1, 'Unable to delete from CouchDB.');
			}
		
		});
	
	});

});

/**
 * Handles requests for user documents, according to the following requirements:
 *
 * Request validators:
 *	1. The current client must be authenticated.
 *
 * The response provided by this handler will be according to the following
 * schema, as a "application/json" encoded string:
 *
 *	{
 *		*success: boolean,
 *		user: object,
 *		error: {
 *			code: int,
 *			msg: string
 *		}
 *	}
 */
$.app.handle('GET', '/user/%w', ['smile-session'], function(uuid) {

	// Find the requested user profile
	this.find('user', uuid, this, function(record) {
	
		// Filter the user fields as requested
		var user = sanitize_user_document.call(this, record);
		
		// Send the user
		this.response.send(200, {
			success: true,
			user: user
		});
	
	});

});
