
$.using(
	'application.modules.schema.institution'
);

/**
 * Returns an error status, code and message based on the given
 * validation report.
 *
 * @param object report
 *	The validation report.
 *
 * @return mixed[]
 *	An array containing the response status, code and message.
 */
function build_validation_report(report) {
	
	// Build the error response
	var validators = report.error.byValidators;
	var code, message;
	
	if(validators.required) {
		
		code = 1;
		message = 'Missing required fields in request: ["' + validators.required.join('", "') + '"]';
		
	} else if(validators.unique) {
		
		if(validators.unique.indexOf('username') > -1) {
		
			code = 4;
			message = 'Username already exists.';
			
		} else {
		
			code = 5;
			message = 'Email address already exists.';
			
		}
		
	} else {
	
		code = 2;
		message = 'Input validation error: ["' +
			report.error.attributes.join('", "') + '"]';
			
	}
	
	return [ 400, code, message ];
}

/**
 * Sends an error response according to the error report.
 *
 * @param object report
 *	The validation error report to build the response from.
 */
function send_validation_report(report) {

	// Send the error response
	var report = build_validation_report.call(this, report);
	this.response.sendError(report[0], report[1], report[2]);
	
}

/**
 * Handles institution create requests.
 */
$.app.handle('POST', '/institution', ['smile-session', 'smile-input'], function() {

	// Get the current session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Get the input data
	var input = this.request.body;
	
	// Validate the user input data
	$.schemaInstitution.validator.validate('POST', input, 
			this, function(report, document) {
		
		// Report any validation errors
		if(!report.success) {
			send_validation_report.call(this, report, document);
			return;
		}
		
		// Build the document for CouchDB
		document = $.object.extend({}, $.schemaInstitution.template, document);
		
		// Add createdOn and creator
		var now = new Date();
		document.creator = me;
		document.createdOn = [
			now.getFullYear(),
			now.getMonth() + 1,
			now.getDate(),
			now.getHours(),
			now.getMinutes(),
			now.getSeconds()
		];
		
		// Store the document in CouchDB
		this.couchDB.store(document, this, function(success, id) {
		
			if(success) {
				this.response.sendUUID(id);
			} else {
				$.error('InstitutionModule: failed to store document in couchdb.');
				this.response.sendError(500, -1, 'Internal server error.');
			}
		
		});
	});

});

/**
 * Handles institution update requests.
 *
 * @param string uuid
 *	The unique document identifier.
 */
$.app.handle('PUT', '/institution/%w', ['smile-session', 'smile-input'], function(uuid) {

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Find the given institution
	this.find('institution', uuid, this, function(record) {
	
		// Get the input data
		var input = this.request.body;
		input._id = record._id;
	
		// Validate the institution input data
		$.schemaInstitution.validator.validate('PUT', input, 
				this, function(report, document) {
		
			// Report any validation errors
			if(!report.success) {
				send_validation_report.call(this, report, document);
				return;
			}
		
			// Build the document for CouchDB
			document = $.object.extend({}, $.schemaInstitution.template, record, document);
		
			// Store the document in CouchDB
			this.couchDB.store(document, this, function(success, id) {
		
				if(success) {
					this.response.sendUUID(id);
				} else {
					$.error('InstitutionModule: failed to store document in couchdb.');
					this.response.sendError(500, -1, 'Internal server error.');
				}
		
			});
			
		});
	
	});

});

/**
 * Handles institution delete requests.
 */
$.app.handle('DELETE', '/institution/%w', ['smile-session'], function(uuid) {

	// Get the session data
	var session = this.session.data;
	var me = session.id_user;
	
	// Find the user account
	this.find('institution', uuid, this, function(record) {
		
		if(!session.admin && record.creator !== me) {
			// Admin authorization required to delete an institution created by 
			// a different user.
			this.response.sendError(401, 7, 'Admin authorization required to call this function.');
			
			return;
		}
		
		this.couchDB.delete(record, this, function(success) {

			if(success) {
				// Send the OK response
				this.response.sendOK();
			} else {
				this.response.sendError(500, -1, 'Unable to delete from CouchDB.');
			}
		
		});
	
	});

});

/**
 * Handles institution fetch requests.
 *
 * @param string uuid
 *	The unique institution identifier.
 */
$.app.handle('GET', '/institution/%w', ['smile-session'], function(uuid) {

	// Find the requested institution profile
	this.find('institution', uuid, this, function(record) {
	
		// Filter the institution fields as requested
		var institution = this.sanitize(record);
		institution.UUID = record._id;
		
		// Send the institution
		this.response.send(200, {
			success: true,
			institution: institution
		});
	
	});

});

/**
 * Handles institution fetch by name requests.
 *
 * @param string name
 *	The name of the institution to fetch.
 */
$.app.handle('GET', '/institution/getByName/%s', ['smile-session'], function(name) {

	// Get the institution document by name
	this.couchDB.get('institution', 'by_name', { key: name }, this, function(records) {

		var record = records[0];
		if(!record) {
		
			this.response.send(404, {
				success: false,
				error: {
					code: 13,
					msg: 'No Institution found with name: ' + name
				}
			});

			return;
		}
		
		var institution = this.sanitize(record);
		
		this.response.send(200, {
			success: true,
			institution: institution
		});
			
	});
});

$.app.handle('POST', '/institution/getNamesFromUUID', ['smile-session', 'smile-input'], function() {

	// Validate the input data
	var input = this.request.body;
	var uuids = input.UUIDs;
	
	if(!(uuids instanceof Array)) {
		this.response.sendError(400, 1, 'Missing required fields in request: ["UUIDs"]');
		return;
	}
	
	// Get the requested users by id
	this.couchDB.get('institution', 'by_id', { keys: uuids }, this, function(records) {
		
		var results = [];
		
		// Build the results array
		for(var i in records) {
			results.push({
				name: records[i].name,
				UUID: records[i]._id
			});
		}
		
		this.response.send(200, {
			success: true,
			institutions: results
		});

	});
	
});

$.app.handle('GET', '/institution/nameAutoFill', ['smile-session'], function() {
	var name = this.request.query['name'];
	
	if(!name) {
		this.response.sendError(400, 1, 'Missing required fields in request: ["name"]');
		return;
	}
	
	/* ... */
});

