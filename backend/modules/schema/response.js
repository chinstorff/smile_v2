/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

$.using(

	'system.validator'
	
);

module.exports = {

	// Module id
	_id: 'schemaResponse',

	// Validator instance	
	validator: $.validator.create([
	
		// Unsafe attributes for all events
		['unsafe', 'creator,photo'],
	
		// Required attributes during POST
		['required', 'userId,resourceId', 'POST'],
		
		// Arrays
		['number-array', 'answerChoices']
	
	]),
	
	// Document template
	template: {
		doctype: 'response',
	
		userId: undefined,
		resourceId: undefined,
		sessionId: undefined,
		answerChoice: undefined,
		responseValue: undefined,
		responseResource: undefined,
		secondsToAnswer: undefined,
		rating: undefined
	}

};

