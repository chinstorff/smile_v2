/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

/**
 * Requires user admin authentication in order to validate this request.
 *
 * @param HttpMessage request
 *	The instance of the request being validated.
 *
 * @param HttpResponse response
 *	The response associated with the request.
 *
 * @param HttpSession session
 *	The session associated with the request.
 */
$.app.setCustomValidator('smile-admin', function(request, response, session) {

	if(!session.data.admin) {
		this.sendError(401, 7, 'Admin authorization required to call this function.');
		return false;
	}
	
	return true;

});