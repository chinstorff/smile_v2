/**
 * Copyright (C) SMILE Consortium 2013,
 * Developed by Neticle Portugal
 */

/**
 * Requires user authentication in order to validate this request.
 *
 * @param HttpMessage request
 *	The instance of the request being validated.
 *
 * @param HttpResponse response
 *	The response associated with the request.
 *
 * @param HttpSession session
 *	The session associated with the request.
 */
$.app.setCustomValidator('smile-session', function(request, response, session) {

	if(!session.data.authenticated) {
		response.sendError(403, 8, 
			'Authorization is required. You must be logged in as a SMILE user to call this function.'
		);
		
		return false;
	}
	
	return true;

});
