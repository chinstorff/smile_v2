$.ajax({

    url: 'http://localhost:1337/response',
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    
    data: JSON.stringify({
		userId: "d822d9eeb077dfcba1c564df80003419",
		resourceId: "0ae944c4526d4d1946bb02d228006232", //resource that response is in
		sessionId: "a7db4a8031f018d803458d2c56000c6d", // activity = 0ae944c4526d4d1946bb02d2280074bf
		answerChoice: [ 0 ], //must be an array of integers, min length of 1
		responseValue: ["cola", "bears"], //can be any data-type
		responseResource: "asdf3q4gavserg", //can attach a resource as a response
		secondsToAnswer: 12.3, //seconds, > 0, number
		rating: 3.2 //rating >= 1 and <= 5, can be decimal
	})

});
