$.ajax({

	url: '/group/dd334ec6e5c40e0a959340e045002353/removeMembers',
	type: 'PUT',
	dataType: 'json',
	contentType: 'application/json',
	
	data: JSON.stringify({
		members: [
			{ UUID: 'member-test-1' },
			{ UUID: 'member-test-2' },
			{ UUID: 'member-test-3' }
		]
	})

});
