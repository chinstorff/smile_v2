$.ajax({

    url: 'http://localhost:1337/user',
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    
    data: JSON.stringify({
		username: "pbispo",
		password: "smile@neticle",
		firstName: "Pedro",
		lastName: "Bispo",
		emailAddresses: [ "pedro.bispo@neticle.com", "pedro.bispo@neticle.pt" ],
		admin: false,
		country: "Portugal",
		region: "Ilha Terceira, Azores",
		city: "Praia da Vitória",
		roles: ["student"],
		grade: 12,
		institutions: ["1234i1288", "12349001"],
		language: "English"
	})

});

$.ajax({

    url: 'http://localhost:1337/user',
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    
    data: JSON.stringify({
		username: "user0",
		password: "testPassword9901",
		firstName: "Test",
		lastName: "User",
		emailAddresses: [ "test.zero@smileconsortium.com" ],
		admin: false,
		country: "Portugal",
		region: "Ilha Terceira, Azores",
		city: "Praia da Vitória",
		roles: ["student"],
		grade: 12,
		institutions: ["1234i1288", "12349001"],
		language: "English"
	})

});

/*

POST /user HTTP/1.1
Host: localhost:1337
Connection: keep-alive
Content-Length: 344
Accept: application/json, text/javascript, *\/*; q=0.01
Origin: http://localhost:1337
X-Requested-With: XMLHttpRequest
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.65 Safari/537.36
Content-Type: application/json
Referer: http://localhost:1337/
Accept-Encoding: gzip,deflate,sdch
Accept-Language: en-US,en;q=0.8

{"username":"pbispo","password":"smile@neticle","firstName":"Pedro","lastName":"Bispo","emailAddresses":["pedro.bispo@neticle.com","pedro.bispo@neticle.pt"],"admin":false,"country":"Portugal","region":"Ilha Terceira, Azores","city":"Praia da VitÃ³ria","roles":["student"],"grade":12,"institutions":["1234i1288","12349001"],"language":"English"}



HTTP/1.1 200 OK
Set-Cookie: session=954aca9c365b29f05605b8c9db10e205631ca83f; domain=; path=/
Content-Type: application/json
Transfer-Encoding: chunked
Access-Control-Allow-Origin: *
Access-Control-Allow-Headers: origin, content-type, accept
Connection: keep-alive

{"success":true,"UUID":"0cbc6dc62a8f09f1b7b6a51885003164"}

*/
