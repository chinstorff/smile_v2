$.ajax({

    url: 'http://localhost:1337/resource',
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    
    data: JSON.stringify({
		UUID: "asdv3g2rg2rg2rg",
		name:'My own resource.',
		resourceType: 'flippedQuestion',
		owner: "asdf2egrsdf43g",
		creator: "asdf2egrsdf43g",
		activityIds: ["0ae944c4526d4d1946bb02d22800351e"],
		attachments: [ 
		
			{
				title: 'My Plaintext Resource',
				name: 'myresource.txt',
				mime: 'plain/text',
				attachment: 'SSBhbSBhIHBsYWluIHRleHQgcmVzb3VyY2Uu' // I am a plain text resource.
			}
		
		],
		title: "What is 3 + 3?",
		description: "",
		answerChoices: [ 
		
			{
				pos: 5,
				text: 'This is option B, which is correct.',
				correct: true,
				response: 'You are good to go!'
			},
			
			{
				pos: 2,
				text: 'This is option A, before B and it\'s wrong.',
				correct: false,
				response: 'You should study some more.'
			}
		
		],
		multiSelect: false,
		allowMultipleResponses: false,
		allowCreatorResponse: false,
		allowResponseRevision: false,
		public: true,
		visible: true,
		tags: ["science", "grade:12", "chemistry", "AP chemistry", "AP chem", "atomic chemistry", "orbitals"],
		gradeLevels: ["12"],
		subjects: ["chemistry", "science", "physical sciences"],
		timed: false,
		timerSecs: 60,
		createdOn: [2009, 1, 31, 18, 4, 11],
		language: "English"
	})

});
