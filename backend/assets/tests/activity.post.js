$.ajax({

	url: '/activity',
	type: 'POST',
	dataType: 'json',
	contentType: 'application/json',
	
	data: JSON.stringify({
		name: "Ed Tech Mobile Innovations",
		description: "",
		activityType: "flippedMC",
		owner: "asdfaa21wd123333f",
		owningGroup: "464261199fa310f48bbdb74939000e48",
		institution: "12341234",
		'public': true,
		tags: ["science", "grade:12", "chemistry", "AP chemistry", "AP chem", "atomic chemistry", "orbitals"],
		gradeLevels: ["12"],
		subjects: ["chemistry", "science", "physical sciences"],
		// photo: [binary photo data],
		language: "English"
	})

});
