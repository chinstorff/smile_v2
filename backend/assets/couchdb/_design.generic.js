/* by_id_doctype */
function(d) {

	// Extract the doctype with a fallback value and emit the document
	var doctype = d.doctype ? d.doctype : 'undefined';
	emit([ d._id, doctype ], d);
	
}