/* group.by_id */
function(d) {

	if(d.doctype === 'group') {
		emit(d._id, d);
	}

}

/* group.by_name */
function(d) {

	if(d.doctype === 'group') {
		emit(d.name, d);
	}
	
}

/* group.by_owner */
function(d) {

	if(d.doctype === 'group') {
		emit(d.owner, d);
	}
	
}

/* group.by_organizer */
function(d) {

	if(d.doctype === 'group') {
		
	}

}

/* group.by_owner_organizer */
function(d) {
	
	if(d.doctype === 'group') {
		
		// Emit the document indexed by owner
		emit({ owner: d.owner }, d);
		
		// Emit the document indexed by organizers
		if(d.organizerIds) {
		
			for(var i = 0, collection = d.organizerIds, length = collection.length;
					i < length; ++i) {
				
				emit({ organizer: collection[i] }, d);
					
			}
		
		}
		
	}
	
}

/* group.by_invitee */
function(d) {

	if(d.doctype === 'group') {
		
		// Emit the document indexed by invitees
		if(d.invitedIds) {
		
			for(var i = 0, collection = d.invitedIds, length = collection.length;
					i < length; ++i) {
				
				emit(collection[i], d);
					
			}
		
		}
	
	}
	
}

/* group.by_member */
function(d) {

	if(d.doctype === 'group') {
	
		// Determine the public status
		var gpublic = d['public'] ? true : false;
		
		// The owner is a member too
		emit([d.owner, gpublic], d);
		
		// Emit the document indexed by memberIds
		if(d.memberIds) {
		
			for(var i = 0, collection = d.memberIds, length = collection.length;
					i < length; ++i) {
				
				emit([collection[i], gpublic], d);
					
			}
		
		}
	
	}
	
}

