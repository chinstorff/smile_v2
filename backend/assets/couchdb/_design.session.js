/* session.by_id */
function(d) {

	if(d.doctype === 'session') {
		emit(d._id, d);
	}

}

/* session.by_group */
function(d) {

    if(d.doctype === 'session') {
        emit([d.group, d.visible], d);
    }

}

/* session.by_activity */
function(d) {

    if(d.doctype === 'session') {
        emit([d.activity, d.visible], d);
    }

}

/* session.by_group_activity */
function(d) {

    if(d.doctype === 'session') {
        emit([d.group, d.activity, d.visible], d);
    }

}