/* Provides function to convert from an array of objects with UUIDs and other vars to one identified by UUID
 *
 * var institutions = [
 *  {UUID: "ce93385393dd30e2c8eb327b9a25b9ec", name: "Gotham"},
 *  {UUID: "ce93385393dd30e2c8eb327b9a25b998", name: "SMILEConsortium"}
 * ]
 * _.listToObj(j);
 * => { ce93385393dd30e2c8eb327b9a25b9ec: "Gotham", ce93385393dd30e2c8eb327b9a25b998: "SMILEConsortium" } */

 _.listToObj = function (list, varName) {
    var obj = {};
    _.map(list, function (i) {
        obj[i.UUID] = i[varName];
    });
    return obj;
}

