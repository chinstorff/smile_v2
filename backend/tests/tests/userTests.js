window.userTests = function(callback) {

    state.user0 = { firstName: "User", lastName: "Zero", username: "user0", email: "servicesTest0@test.net", password: "testPassword9901"};
    state.user1 = { firstName: "User", lastName: "One", username: "user1", email: "test1@test.net", password: "testPassword9901"};
    state.user2 = { firstName: "User", lastName: "Two", username: "user2", email: "test2@test.net", password: "testPassword9901"};
    state.user3 = { firstName: "User", lastName: "Three", username: "user3", email: "test3@test.net", password: "testPassword9901"};
    state.user4 = { firstName: "User", lastName: "Four", username: "user4", email: "test4@test.net", password: "testPassword9901"};
    state.user5 = { firstName: "User", lastName: "Five", username: "user5", email: "test5@test.net", password: "testPassword9901"};
    state.user6 = { firstName: "User", lastName: "Six", username: "user6", email: "test6@test.net", password: "testPassword9901"};
    state.user7 = { firstName: "User", lastName: "Seven", username: "user7", email: "test7@test.net", password: "testPassword9901"};
    state.user8 = { firstName: "User", lastName: "Eight", username: "user8", email: "test8@test.net", password: "testPassword9901"};
    state.user9 = { firstName: "User", lastName: "Nine", username: "user9", email: "test9@test.net", password: "testPassword9901"};
    state.user10 = { firstName: "User", lastName: "Ten", username: "user10", email: "test10@test.net", password: "testPassword9901"};
    state.user11 = { firstName: "User", lastName: "Eleven", username: "user11", email: "test11@test.net", password: "testPassword9901"};
    state.photoUser = { username: "photoUser", email: "picassa@photo.org", password: "iLikePhotos89"};

    servicesTest('logout', 'auth/logout', 'POST', null, {}, true, "success", function () {
        //delete test users to make sure they don't exist
        var users = [];
        for (var i = 1; i <= 11; i++) {
            users[i] = state["user" + i];
        }

        function deleteUsers(userList) {
            var user = userList.pop();
            if (userList.length <= 0) {
                createUser0();
                return;
            }
            //var date = new Date();
            //console.log("Calling function, delete " + user.username + ": " + (date.getTime() - startTime));
            deleteUser(user, function () {
                //var date = new Date();
                //console.log("function completed, delete " + user.username + ": " + (date.getTime() - startTime));
                deleteUsers(userList);
            });
        }

        login(state.user0, function () {
            //deleteUser(state.photoUser, function() {
            deleteUsers(users);
            //});
        });

    });

    function createUser0 () { 
        servicesTest('create user','user', 'POST', null, {
            username: state.user1.username,
            password: state.user1.password,
            firstName: "Pedro",
            lastName: "Bispo",
            emailAddresses: [ state.user1.email ],
            admin: false,
            country: "Portugal",
            region: "Ilha Terceira, Azores",
            city: "Praia da Vitória",
            roles: ["student"],
            grade: 12,
            institutions: ["1234i1288", "12349001"],
            language: "English"
        }, true, "success", function(response) {
            test('create user: returns UUID', function() {
                var varname = "UUID";
                ok(response[varname], varname + ": " + response[varname]);
                var varname = "success";
                equal(response[varname], true, varname + ": " + response[varname]);
                state.user1.UUID = response.UUID;

                updateUser0();
            })
        });
    }
    function updateUser0 () {
        var UUID = state.user1.UUID;
        servicesTest('update user','user/' + UUID, 'PUT', state.user1, {
            country: "United States",
            crazyNewVar: 3006
        }, true, "success", updateUser1);
    }
    function updateUser1() {
        //check for update country
        var UUID = state.user1.UUID;
        servicesTest('check if user has updated info','user/' + UUID, 'GET', state.user1,
            null, "United States", "user.country", function(response) {
                test('update user check addition of new field', function() {
                    equal(response.user.crazyNewVar, 3006);
                    createUser1(); 
                });
            });
    }
    function createUser1 () { 
        servicesTest('create second user','user', 'POST', null, {
            username: state.user2.username,
            password: state.user2.password,
            firstName: "Noah",
            lastName: "Freedman",
            emailAddresses: [ state.user2.email ],
            admin: false,
            country: "United States",
            region: "California",
            city: "Carmel",
            roles: ["student"],
            grade: 12,
            institutions: [],
            language: "English"
        }, true, "success", function(response) {
            state.user2.UUID = response.UUID;
            getUser();
        });
    }
    function getUser() { 
        servicesTest('get user','user/' + state.user1.UUID + "?onlyFields=username", 'GET', state.user2, {
        }, state.user1.username, "user.username", function(response, objString) {
            console.log(response);
            console.log(objString);
            test('get user: only returns username since onlyFields is passed', function() {
                console.log(response);
                equal(typeof(response.user.emailAddresses), 'undefined', 
                    'Only specified fields are returned by GET when onlyFields parameter is passed' + objString);
                getByUsername0();  
            })
        });
    }
    function getByUsername0 () { 
        servicesTest('get user/getByUsername username matches', 'user/getByUsername/' + state.user0.username, 'GET', state.user2, 
            null, "user0", "user.username", function(response) {
                console.log(response);
                state.user0.UUID = response.user.UUID;
                getByUsername1();
            });
    }
    function getByUsername1 () { 
        servicesTest('get user/getByUsername grade matches', 'user/getByUsername/' + state.user1.username, 'GET', state.user2, null, 12, "user.grade", getByUsername2);
    }
    function getByUsername2 () {
        servicesTest('get user with photo','user/getByUsername/' + 'photoUser', 'GET', state.user2, {
        }, true, "success", function(response) {
            usernameUnique0();
        });
    }
    function usernameUnique0() { 
        servicesTest('username is unique false','user/usernameIsUnique?username=' + state.user1.username, 'GET', "", {
        }, false, "unique", function(response) {
            usernameUnique1();
        });
    }
    function usernameUnique1() { 
        servicesTest('username is unique true','user/usernameIsUnique?username=' + 'uniqueUser', 'GET', "", {
        }, true, "unique", function(response) {
            usernameUnique2();
        });
    }
    function usernameUnique2() { 
        servicesTest('username unique accesible without login','user/usernameIsUnique?username=' + state.user1.username, 'GET', "", {
        }, true, "success", function(response) {
            emailUnique0();
        });
    }
    function emailUnique0() { 
        servicesTest('email is unique false','user/emailIsUnique?email=' + state.user1.email, 'GET', state.user2, {
        }, false, "unique", function(response) {
            emailUnique1();
        });
    }
    function emailUnique1() { 
        servicesTest('email is unique true','user/emailIsUnique?email=' + 'uniqueEmail', 'GET', state.user2, {
        }, true, "unique", function(response) {
            emailUnique2();
        });
    }
    function emailUnique2() {
        servicesTest('email unique accesible without login','user/emailIsUnique?email=' + state.user1.email, 'GET', "", {
        }, true, "success", function(response) {
            getUsernamesFromUUID0();
        });
    }
    function getUsernamesFromUUID0() { 
        servicesTest('get usernames from UUID without authentication','user/getUsernamesFromUUID', 'POST', state.user2, {
            UUIDs: [ state.user0.UUID, state.user1.UUID, state.user2.UUID, "Doesntexist" ]
        }, false, "success", function(response, objString) {
            console.log(response);
            test('get usernames from UUID: Error Code 9', function() {
                console.log(response);
                equal(response.error.code, 9, objString);
                getUsernamesFromUUID1();
            });
        });
    }
    function getUsernamesFromUUID1() { 
        servicesTest('get usernames from UUID','user/getUsernamesFromUUID', 'POST', null, {
            UUIDs: [ state.user0.UUID, state.user1.UUID, state.user2.UUID ]
        }, true, "success", function(response) {
            test('get usernames from UUID: usernames are correct', function() {
                console.log(response);
                if (response && response.users) {
                    equal(response.users[0].username, 'user0', 'user0 found');
                    equal(response.users[1].username, 'user1', 'user1 found');
                    equal(response.users[2].username, 'user2', 'user2 found');
                }
                batchAddUsers0();
            })
        });
    }
    function batchAddUsers0() { 
        servicesTest('batch add users','user/batchAdd', 'POST', state.user1, {
            users: [
                { firstName: state.user3.firstName, lastName: state.user3.lastName, username: state.user3.username, password: state.user3.password, emailAddresses: [state.user3.email] },
                { firstName: state.user4.firstName, lastName: state.user4.lastName,  username: state.user4.username, password: state.user4.password, emailAddresses: [state.user4.email] },
                { firstName: state.user5.firstName, lastName: state.user5.lastName,  username: state.user5.username, password: state.user5.password, emailAddresses: [state.user5.email] },
                { firstName: state.user6.firstName, lastName: state.user6.lastName,  username: state.user6.username, password: state.user6.password, emailAddresses: [state.user6.email] },
                { firstName: state.user7.firstName, lastName: state.user7.lastName,  username: state.user7.username, password: state.user7.password, emailAddresses: [state.user7.email] },
                { firstName: state.user8.firstName, lastName: state.user8.lastName,  username: state.user8.username, password: state.user8.password, emailAddresses: [state.user8.email] },
                { firstName: state.user9.firstName, lastName: state.user9.lastName,  username: state.user9.username, password: state.user9.password, emailAddresses: [state.user9.email] },
                { firstName: state.user10.firstName, lastName: state.user10.lastName,  username: state.user10.username, password: state.user10.password, emailAddresses: [state.user10.email] },
                { firstName: state.user11.firstName, lastName: state.user11.lastName,  username: state.user11.username, password: state.user11.password, emailAddresses: [state.user11.email] }
            ]
        }, true, "success", function(response) {
            notEqual(response.UUIDs[0], null, 'First user created and UUID returned');
            for (var i = 3; i <= 11; i++) {
                if (response.UUIDs && response.UUIDs[i - 3])
                state["user" + i].UUID = response.UUIDs[i - 3];
            }
            callback && callback();
        });
    }
}