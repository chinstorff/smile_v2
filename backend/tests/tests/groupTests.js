window.groupTests = function (callback) {

    //By end of tests, group0 is owned by user2 and has organizers user1, user2, and user5.
    //By end of group tests, group0 has members user1, user2, user3, user4, user5, user6, user7, user8.

    //By end of tests, group1 is owned by user7 and has organizer user5 and user7.
    //By end of group tests, group1 has members user5, user7, user8, user9, user10.

    state.group0 = { name: "EdTech" };
    state.group1 = { name: "AlgebraMrRyan" };
    //check if group0 exists
    login(state.user2, function () {
        if (!state.group0.UUID) {
            $.ajax({
                url: server + "group/getByName/" + state.group0.name,
                type: 'GET',
                complete: function (result, status) {
                    if (status == "success") {
                        var response = JSON.parse(result.responseText);
                        state.group0.UUID = response.group.UUID;

                        deleteGroup0(response.group.UUID);
                    } else {
                        getGroup1ToDelete();
                    }
                }
            });
        } else {
            deleteGroup0(state.group0.UUID);
        }
    });

    function deleteGroup0(UUID) {
        servicesTest('delete group - admin', 'group/' + UUID, 'DELETE', state.user0, {
        }, true, "success", function (response) {
            getGroup1ToDelete();
        });
    }

    function getGroup1ToDelete() {
        //get next group name to delete
        if (!state.group1.UUID) {
            $.ajax({
                url: server + "group/getByName/" + state.group1.name,
                type: 'GET',
                complete: function (result, status) {
                    if (status == "success") {
                        var response = JSON.parse(result.responseText);
                        state.group1.UUID = response.group.UUID;

                        deleteGroup1(response.group.UUID);
                    } else {
                        createGroup0();
                    }
                }
            });
        } else {
            deleteGroup1(state.group1.UUID);
        }
    }

    function deleteGroup1(UUID) {
        servicesTest('delete group', 'group/' + UUID, 'DELETE', state.user0, {
        }, true, "success", function (response) {
            createGroup0();
        });
    }

    function createGroup0() {
        var group = state.group1;
        servicesTest('create group1 - non-admin user', 'group', 'POST', state.user7,
            {
                name: group.name,
                groupType: "standard",
                institution: "12341234",
                owner: state.user7.UUID,
                memberIds: [ state.user5.UUID, state.user8.UUID, state.user9.UUID, state.user10.UUID ],
                organizerIds: [ state.user7.UUID ],
                invitedIds: [ state.user11.UUID ],
                public: false,
                requirePasscode: false,
                passcode: "smileLearning",
                tags: ["math", "grade:10", "algebra"],
                gradeLevels: ["10"],
                subjects: ["math", "algebra"],
                language: "English"
            },
            true,
            "success",
            function (response) {
                if (response && response.UUID) state.group1.UUID = response.UUID;
                createGroup1();
            }
        );
    }

    function createGroup1() {
        var group = state.group0;
        servicesTest('create group0', 'group', 'POST', state.user0,
            {
                name: group.name,
                groupType: "standard",
                institution: "12341234",
                owner: state.user2.UUID,
                public: true,
                requirePasscode: true,
                passcode: "smileLearning",
                tags: ["science", "grade:12", "chemistry", "AP chemistry", "AP chem", "atomic chemistry", "orbitals"],
                gradeLevels: ["9", "12"],
                subjects: ["chemistry", "science", "physical sciences"],
                language: "English"
            },
            true,
            "success",
            function (response) {
                if (response && response.UUID) state.group0.UUID = response.UUID;
                getByName(state.group0.name);
            }
        );
    }

    function getByName(name) {
        servicesTest('group getByName', 'group/getByName/' + name, 'GET', null,
            null, true, "success",
            function (response) {
                var groupUUID = getPropByString(response, "group.UUID");
                state.group0 = response.group;

                test('check owner is added to organizerIds', function () {
                    equal(state.group0.organizerIds && state.group0.organizerIds[0], state.user2.UUID, state.group0.organizerIds);
                    updateGroup();
                });
            }
        );
    }

    function updateGroup() {
        servicesTest('update group', 'group/' + state.group0.UUID, 'PUT', state.user0, {
            groupType: "onetime",
            institution: "56456775",
            invitedIds: [ state.user1.UUID, state.user2.UUID, state.user11.UUID ],
            memberIds: [ state.user3.UUID, state.user3.UUID, state.user3.UUID, state.user4.UUID ],
            organizerIds: [ state.user5.UUID ],
            public: true,
            requirePasscode: true,
            passcode: "smile2learn",
            tags: ["science", "physics", "chemistry", "orbitals"],
            gradeLevels: ["11"],
            subjects: ["science", "physics"],
            language: "English (US)"
        }, true, "success", function (response) {
            getGroup0();
        });
    }

    function getGroup0() {
        var UUID = state.group0.UUID;
        servicesTest('get group', 'group/' + UUID, 'GET', null, {
        }, true, "success", function (response) {
            test('update user check addition of new field', function () {
                var memberIds = response.group.memberIds;
                var organizerIds = response.group.organizerIds;
                equal(response.group.passcode, "smile2learn");
                equal(organizerIds.indexOf(state.user5.UUID) > -1, true, "Added organizer appears in organizerIds list.");
                equal(organizerIds.indexOf(state.user2.UUID) > -1, true, "Owner  appears in organizerIds list.");
                equal(memberIds.indexOf(state.user5.UUID) > -1, true, "Added organizer also appears in members list.");
                equal(memberIds.indexOf(state.user2.UUID) > -1, true, "Owner also appears in members list.");
                equal(memberIds.indexOf(state.user3.UUID), _.lastIndexOf(memberIds, state.user3.UUID), "Duplicate added user exists only once.");
                state.group0.memberIds = response.group.memberIds;
                state.group0.organizerIds = response.group.organizerIds;
                showGroupMembers0();
            });
        });
    }

    function showGroupMembers0() {
        servicesTest('get group0 members\' usernames from UUID', 'user/getUsernamesFromUUID', 'POST', null, {
            UUIDs: state.group0.memberIds
        }, true, "success", function (response) {
            uniqueGroup0();
        });
    }

    function uniqueGroup0() {
        servicesTest('group nameIsUnique', 'group/nameIsUnique?name=UNIQUEGROUPNAME', 'GET', null,
            null
            , true, "unique", function (response) {
                uniqueGroup1();
            });
    }

    function uniqueGroup1() {
        servicesTest('group nameIsUnique', 'group/nameIsUnique?name=' + state.group0.name, 'GET', null,
            null
            , false, "unique", function (response) {
                addMembers0();
            });
    }

    function addMembers0() {
        servicesTest('addMembers', 'group/' + state.group0.UUID + '/addMembers', 'PUT', state.user5, {
            members: [
                { UUID: state.user2.UUID, organizer: false },
                { UUID: state.user5.UUID, organizer: false },
                { UUID: state.user6.UUID, organizer: true },
                { UUID: state.user6.UUID, organizer: true },
                { UUID: state.user7.UUID, organizer: false }
            ]
        }, true, "success", function (response) {
            removeMembers0();
        });
    }

    function removeMembers0() {
        servicesTest('removeMembers - organizer removing member', 'group/' + state.group0.UUID + '/removeMembers', 'PUT', state.user5, {
            members: [
                { UUID: state.user7.UUID }
            ]
        }, true, "success", function (response) {
            removeMembers1();
        });
    }

    function removeMembers1() {
        servicesTest('removeMembers - organizer removing organizer', 'group/' + state.group0.UUID + '/removeMembers', 'PUT', state.user5, {
            members: [
                { UUID: state.user6.UUID }
            ]
        }, false, "success", function (response) {
            removeMembers2();
        });
    }

    function removeMembers2() {
        servicesTest('organizer can\'t remove group owner',
            'group/' + state.group0.UUID + '/removeMembers', 'PUT', state.user5, {
                members: [
                    { UUID: state.user2.UUID }
                ]
            }, false, "success", function (response) {
                getGroup1();
            });
    }

    function getGroup1() {
        var UUID = state.group0.UUID;
        servicesTest('get group', 'group/' + UUID, 'GET', null, {
        }, true, "success", function (response) {
            var memberIds = response.group.memberIds;
            var organizerIds = response.group.organizerIds;
            equal(memberIds.indexOf(state.user2.UUID), _.lastIndexOf(memberIds, state.user2.UUID), "Duplicate added user exists only once.");
            equal(organizerIds.indexOf(state.user2.UUID), _.lastIndexOf(organizerIds, state.user2.UUID), "Duplicate added organizer exists only once.");
            equal(memberIds.indexOf(state.user6.UUID) > -1, true, "Added user exists.");
            equal(memberIds.indexOf(state.user6.UUID), _.lastIndexOf(memberIds, state.user6.UUID), "Duplicate added user exists only once.");
            equal(organizerIds.indexOf(state.user6.UUID) > -1, true, "Added organizer exists.");
            equal(organizerIds.indexOf(state.user6.UUID), _.lastIndexOf(organizerIds, state.user6.UUID), "Duplicate added organizer exists only once.");
            equal(memberIds.indexOf(state.user7.UUID), -1, "Added but then removed user does not exist.");
            state.group0.memberIds = response.group.memberIds;
            state.group0.organizerIds = response.group.organizerIds;

            showGroupMembers1();
        });
    }

    function showGroupMembers1() {
        servicesTest('get group members\' usernames from UUID', 'user/getUsernamesFromUUID', 'POST', null, {
            UUIDs: state.group0.memberIds
        }, true, "success", function (response) {
            joinGroup0();
        });
    }

    function joinGroup0() {
        var UUID = state.group0.UUID;
        servicesTest('joinGroup - invited, wrong passcode', 'group/' + UUID + '/joinGroup', 'POST', state.user1, {
            passcode: 'smileLearning_WRong'
        }, true, "success", function (response) {
            joinGroup1();
        });
    }

    function joinGroup1() {
        var UUID = state.group0.UUID;
        servicesTest('joinGroup - not invited, wrong passcode', 'group/' + UUID + '/joinGroup', 'POST', state.user7, {
            passcode: 'smileLearning_WRong'
        }, false, "success", function (response) {
            joinGroup2();
        });
    }

    function joinGroup2() {
        var UUID = state.group0.UUID;
        servicesTest('joinGroup - user8', 'group/' + UUID + '/joinGroup', 'POST', state.user8, {
            passcode: 'smile2learn'
        }, true, "success", function (response) {
            joinGroup3();
        });
    }

    function joinGroup3() {
        var UUID = state.group0.UUID;
        servicesTest('joinGroup - user7', 'group/' + UUID + '/joinGroup', 'POST', state.user7, {
            passcode: 'smile2learn'
        }, true, "success", function (response) {
            joinGroup4();
        });
    }

    function joinGroup4() {
        var UUID = state.group0.UUID;
        servicesTest('joinGroup - cannot join a group you are a member of already', 'group/' + UUID + '/joinGroup', 'POST', state.user7, {
            passcode: 'smile2learn'
        }, false, "success", function (response) {
            getGroup2();
        });
    }

    function getGroup2() {
        var UUID = state.group0.UUID;
        servicesTest('get group', 'group/' + UUID, 'GET', null, {
        }, true, "success", function (response) {
            var memberIds = response.group.memberIds;
            var organizerIds = response.group.organizerIds;
            equal(memberIds.indexOf(state.user7.UUID) > -1, true, "Added user exists.");
            equal(memberIds.indexOf(state.user8.UUID) > -1, true, "Added user exists.");
            state.group0.memberIds = response.group.memberIds;
            state.group0.organizerIds = response.group.organizerIds;
            addOrganizers0();
        });
    }

    function addOrganizers0() {
        servicesTest('addOrganizers non-admin user', 'group/' + state.group0.UUID + '/addOrganizers', 'PUT', state.user3, {
            organizers: [
                { UUID: state.user1.UUID }
            ]
        }, false, "success", function (response) {
            addOrganizers1();
        });
    }

    function addOrganizers1() {
        servicesTest('addOrganizers', 'group/' + state.group0.UUID + '/addOrganizers', 'PUT', state.user5, {
            organizers: [
                { UUID: state.user1.UUID }
            ]
        }, true, "success", function (response) {
            addOrganizers2();
        });
    }

    function addOrganizers2() {
        servicesTest('addOrganizers that are already group members', 'group/' + state.group0.UUID + '/addOrganizers', 'PUT', state.user5, {
            organizers: [
                { UUID: state.user7.UUID },
                { UUID: state.user4.UUID }
            ]
        }, true, "success", function (response) {
            addOrganizers3();
        });
    }

    function addOrganizers3() {
        servicesTest('addOrganizers previous non-admin user after added to group', 'group/' + state.group0.UUID + '/addOrganizers', 'PUT', state.user1, {
            organizers: [
                { UUID: state.user1.UUID }
            ]
        }, true, "success", function (response) {
            addOrganizers4();
        });
    }

    function addOrganizers4() {
        servicesTest('addOrganizers as admin user', 'group/' + state.group0.UUID + '/addOrganizers', 'PUT', state.user0, {
            organizers: [
                { UUID: state.user8.UUID }
            ]
        }, true, "success", function (response) {
            getGroup3();
        });
    }

    function getGroup3() {
        var group = state.group0;
        var UUID = group.UUID;
        servicesTest('get group0', 'group/' + UUID, 'GET', null, {
        }, true, "success", function (response) {
            var memberIds = response.group.memberIds;
            var organizerIds = response.group.organizerIds;
            equal(organizerIds.indexOf(state.user1.UUID) > -1, true, "Added organizer exists.");
            equal(organizerIds.indexOf(state.user1.UUID), _.lastIndexOf(organizerIds, state.user1.UUID), "Duplicate added organizer exists only once.");
            equal(memberIds.indexOf(state.user1.UUID) > -1, true, "Added user exists.");
            equal(memberIds.indexOf(state.user1.UUID), _.lastIndexOf(memberIds, state.user1.UUID), "Duplicate added user exists only once.");
            equal(organizerIds.indexOf(state.user2.UUID) > -1, true, "Previous organizer exists.");
            equal(organizerIds.indexOf(state.user5.UUID) > -1, true, "Previous organizer exists.");
            equal(organizerIds.indexOf(state.user4.UUID) > -1, true, "New organizer exists.");
            equal(organizerIds.indexOf(state.user7.UUID) > -1, true, "New organizer exists.");
            equal(organizerIds.indexOf(state.user8.UUID) > -1, true, "New organizer exists.");
            group.memberIds = response.group.memberIds;
            group.organizerIds = response.group.organizerIds;

            showOrganizers0();
        });
    }

    function showOrganizers0() {
        var group = state.group0;
        servicesTest('get group0 organizer\'s usernames from UUID', 'user/getUsernamesFromUUID', 'POST', null, {
            UUIDs: group.organizerIds
        }, true, "success", function (response) {
            removeOrganizers0();
        });
    }

    function removeOrganizers0() {
        servicesTest('removeOrganizers - non-organizer group member', 'group/' + state.group0.UUID + '/removeOrganizers', 'PUT', state.user3, {
            organizers: [
                { UUID: state.user1.UUID },
                { UUID: state.user7.UUID },
                { UUID: state.user8.UUID }
            ]
        }, false, "success", function (response) {
            removeOrganizers1();
        });
    }

    function removeOrganizers1() {
        servicesTest('removeOrganizers - group organizer', 'group/' + state.group0.UUID + '/removeOrganizers', 'PUT', state.user5, {
            organizers: [
                { UUID: state.user1.UUID },
                { UUID: state.user7.UUID }
            ]
        }, true, "success", function (response) {
            removeOrganizers2();
        });
    }

    function removeOrganizers2() {
        servicesTest('removeOrganizers - owner', 'group/' + state.group0.UUID + '/removeOrganizers', 'PUT', state.user2, {
            organizers: [
                { UUID: state.user8.UUID }
            ]
        }, true, "success", function (response) {
            removeOrganizers3();
        });
    }

    function removeOrganizers3() {
        servicesTest('removeOrganizers - admin', 'group/' + state.group0.UUID + '/removeOrganizers', 'PUT', state.user0, {
            organizers: [
                { UUID: state.user4.UUID },
                { UUID: state.user6.UUID }
            ]
        }, true, "success", function (response) {
            getGroup4();
        });
    }

    function getGroup4() {
        var group = state.group0;
        var UUID = group.UUID;
        servicesTest('get group0', 'group/' + UUID, 'GET', null, {
        }, true, "success", function (response) {
            var memberIds = response.group.memberIds;
            var organizerIds = response.group.organizerIds;
            equal(organizerIds.indexOf(state.user1.UUID), -1, "Removed organizer user1 is no longer in organizers list.");
            equal(organizerIds.indexOf(state.user4.UUID), -1, "Removed organizer user6 is no longer in organizers list.");
            equal(organizerIds.indexOf(state.user6.UUID), -1, "Removed organizer user6 is no longer in organizers list.");
            equal(organizerIds.indexOf(state.user7.UUID), -1, "Removed organizer user7 is no longer in organizers list.");
            equal(organizerIds.indexOf(state.user8.UUID), -1, "Removed organizer user8 is no longer in organizers list.");
            equal(memberIds.indexOf(state.user1.UUID) > -1, true, "Removed organizer user1 remains in members list.");
            equal(memberIds.indexOf(state.user4.UUID) > -1, true, "Removed organizer user4 remains in members list.");
            equal(memberIds.indexOf(state.user6.UUID) > -1, true, "Removed organizer user6 remains in members list.");
            equal(memberIds.indexOf(state.user7.UUID) > -1, true, "Removed organizer user7 remains in members list.");
            equal(memberIds.indexOf(state.user8.UUID) > -1, true, "Removed organizer user8 remains in members list.");
            group.memberIds = response.group.memberIds;
            group.organizerIds = response.group.organizerIds;

            showOrganizers1();
        });
    }

    function showOrganizers1() {
        var group = state.group0;
        servicesTest('get group0 organizer\'s usernames from UUID', 'user/getUsernamesFromUUID', 'POST', null, {
            UUIDs: group.organizerIds
        }, true, "success", function (response) {
            getGroup5();
        });
    }

    function getGroup5() {
        var group = state.group1;
        var UUID = group.UUID;
        servicesTest('get group1', 'group/' + UUID, 'GET', null, {
        }, true, "success", function (response) {
            var memberIds = response.group.memberIds;
            var organizerIds = response.group.organizerIds;
            group.memberIds = response.group.memberIds;
            group.organizerIds = response.group.organizerIds;

            showOrganizers2();
        });
    }

    function showOrganizers2() {
        var group = state.group1;
        servicesTest('get group1 organizer\'s usernames from UUID', 'user/getUsernamesFromUUID', 'POST', null, {
            UUIDs: group.organizerIds
        }, true, "success", function (response) {
            changeOwner0();
        });
    }


    function changeOwner0() {
        servicesTest('change group owner non-admin', 'group/' + state.group0.UUID + '/changeOwner', 'PUT', state.user1, {
            owner: state.user1.UUID
        }, false, "success", function (response) {
            changeOwner1();
        });
    }

    function changeOwner1() {
        servicesTest('change group owner - organizer', 'group/' + state.group0.UUID + '/changeOwner', 'PUT', state.user5, {
            owner: state.user1.UUID
        }, false, "success", function (response) {
            changeOwner2();
        });
    }

    function changeOwner2() {
        servicesTest('change group owner - owner', 'group/' + state.group0.UUID + '/changeOwner', 'PUT', state.user2, {
            owner: state.user1.UUID
        }, true, "success", function (response) {
            changeOwner3();
        });
    }

    function changeOwner3() {
        servicesTest('change group owner - old owner', 'group/' + state.group0.UUID + '/changeOwner', 'PUT', state.user2, {
            owner: state.user2.UUID
        }, false, "success", function (response) {

            //NOT IMPLEMENTED YET
            searchGroups();
        });
    }

    //NOT IMPLEMENTED YET
    function searchGroups() {
        var UUID = state.group0.UUID;
        servicesTest('searchGroups', 'group/searchGroups?query=Ed&orderBy=createdOn&descending=true'
            + '&results=20&page=1', 'GET', null, null, true, "success", function (response) {
            fetchInvites();
        });
    }

    // */

    function fetchInvites() {
        servicesTest('fetchInvites', 'group/fetchInvites', 'GET', state.user11,
            null, true, "success", function (response) {

                equal(_.find(response.groupsInvitedTo, function (obj) {
                    return (obj.UUID == state.group0.UUID);
                }) != undefined, true, "Invitation to group0 was found.");

                equal(_.find(response.groupsInvitedTo, function (obj) {
                    return (obj.UUID == state.group1.UUID)
                }) != undefined, true, "Invitation to group1 was found.");
                fetchMyGroups();
            }
        );
    }

    function fetchMyGroups() {
        servicesTest('fetch my groups', 'group/fetchGroups', 'GET', state.user7, null, true, "success",
            function (response) {
                equal(_.find(response.groups, function (obj) {
                    return (obj.UUID == state.group0.UUID);
                }) != undefined, true, "Membership in group0 was found.");
                equal(_.find(response.groups, function (obj) {
                    return (obj.UUID == state.group1.UUID);
                }) != undefined, true, "Membership in group1 (private) was found.");
                equal(response.groups.length, 2, "Only member of group0 and group1");
                fetchGroups0();
            });
    }

    function fetchGroups0() {
        servicesTest('fetchGroups - different user', 'group/fetchGroups/' + state.user2.UUID, 'GET', state.user3,
            null, true, "success", function (response) {
                equal(_.find(response.groups, function (obj) {
                    return (obj.UUID == state.group0.UUID);
                }) != undefined, true, "Membership in group0 was found.");
                equal(response.groups.length, 1, "Only member of group0");
                fetchGroups1();
            });
    }

    function fetchGroups1() {
        servicesTest('fetchGroups - different user, private groups hidden', 'group/fetchGroups/' + state.user7.UUID, 'GET', state.user3,
            null, true, "success", function (response) {
                equal(_.find(response.groups, function (obj) {
                    return (obj.UUID == state.group0.UUID);
                }) != undefined, true, "Membership in group0 was found.");
                equal(_.find(response.groups, function (obj) {
                    return (obj.UUID == state.group1.UUID);
                }) != undefined, false, "Membership in group1 (private) was not found.");
                equal(response.groups.length, 1, "Only member of group0 found");
                fetchGroups2();
            });
    }

    function fetchGroups2() {
        servicesTest('fetchGroups - admin, private groups hidden', 'group/fetchGroups/' + state.user7.UUID, 'GET', state.user0,
            null, true, "success", function (response) {
                equal(_.find(response.groups, function (obj) {
                    return (obj.UUID == state.group0.UUID);
                }) != undefined, true, "Membership in group0 was found.");
                equal(_.find(response.groups, function (obj) {
                    return (obj.UUID == state.group1.UUID);
                }) != undefined, true, "Membership in group1 (private) was found.");
                equal(response.groups.length, 2, "Only member of group0 and group1");
                callback && callback();
            });
    }
}