(function() {
    //store original backbone sync method
    var sync = Backbone.sync;

    Backbone.sync = function(method, model, options) {
        return sync.apply(this, arguments); //call 'super' sync method
    }
})();