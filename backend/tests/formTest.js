function UserForm() {
	this.username = ko.observable("photoUser");
	this.password = ko.observable("password1");
	this.firstName = ko.observable("Jenson");
	this.lastName = ko.observable("James");
	this.country = ko.observable("United States");
	this.region = ko.observable("Nevada");
	this.city = ko.observable("Las Vegas");
	this.grade = ko.observable(8);
	this.admin = ko.observable(false);
	this.emailAddresses = ko.observableArray(["noahfreedman@gmail.com", "nfreedman@stanford.edu"]);
	this.roles = ko.observableArray([]);
	this.institutions = ko.observableArray([]);
}
ko.applyBindings(new UserForm());
window.jQuery(function() {
	$('input[type="file"]').click();
});