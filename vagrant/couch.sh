#!/usr/bin/env bash

#Create CouchDB database
curl -X PUT http://127.0.0.1:5984/smile

#Load design documents into CouchDB
curl -X PUT http://127.0.0.1:5984/smile/_design/activity --data-binary @/vagrant/backend/assets/couchdb/_design.activity.json
curl -X PUT http://127.0.0.1:5984/smile/_design/generic --data-binary @/vagrant/backend/assets/couchdb/_design.generic.json
curl -X PUT http://127.0.0.1:5984/smile/_design/group --data-binary @/vagrant/backend/assets/couchdb/_design.group.json
curl -X PUT http://127.0.0.1:5984/smile/_design/institution --data-binary @/vagrant/backend/assets/couchdb/_design.institution.json
curl -X PUT http://127.0.0.1:5984/smile/_design/resource --data-binary @/vagrant/backend/assets/couchdb/_design.resource.json
curl -X PUT http://127.0.0.1:5984/smile/_design/response --data-binary @/vagrant/backend/assets/couchdb/_design.response.json
curl -X PUT http://127.0.0.1:5984/smile/_design/session --data-binary @/vagrant/backend/assets/couchdb/_design.session.json
curl -X PUT http://127.0.0.1:5984/smile/_design/user --data-binary @/vagrant/backend/assets/couchdb/_design.user.json

#Create first user
curl -X PUT http://127.0.0.1:5984/smile/user/ -H "Content-Type: application/json" -d '{"doctype": "user", "admin": true, "roles": [     "teacher" ], "language": "English", "hostname": "smileglobal.net", "username": "user0", "password": "83d2f000710ec52e32309baad687199d249c331a", "firstName": "Noah", "lastName": "Freedman", "emailAddresses": [     "nfreedman@smilec.org" ], "country": "United States", "region": "California", "city": "Palo Alto", "grade": "", "institutions": [     "1234i1288",     "12349001" ], "createdOn": [     2013,     12,     2,     0,     13,     12 ], "crazyNewVar": 3006}'