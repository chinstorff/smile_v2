(function () {
    app.lang = {
        get: function (key, params) {
            var dict = this.dict[this.lang];
            if (_.has(dict, key)) {
                if (_.isFunction(dict[key])) {
                    return dict[key](params || null);
                } else {
                    return dict[key];
                }
            } else {
                return undefined;
            }
        },
        lang: "english"
    };
    var dict = app.lang.dict = {};
    dict.english = {
        orderBy: "Order by: ",
        searchText: function (a) {
            switch (a * 1) {
                case 0:
                    return "Newest";
                    break;
                case 1:
                    return "Oldest";
                    break;
                case 2:
                    return "Top-Rated";
                    break;
                case 3:
                    return "Most-Answered";
                    break;
                case 4:
                    return "Hardest";
                    break;
                case 5:
                    return "Easiest";
                    break;
                default:
                    return undefined;
                    break;
            }
        },
        home: "Home",
        public: "Public",
        groups: "Groups",
        messages: "Messages",
        profile: "Profile",
        settings: "Settings",
        about: "About",
        trophy_case: "Trophy Case",
        logOut: "Log Out",
        login: "Login",
        register: "Register",
        signin: "Sign In",
        signinTitle: "Already have a SMILE account?",
        register: "Create New Account",
        registerTitle: "New to SMILE?",
        forgotPassword: "Forgot password?",
        forgotUsername: "Forgot username?",
        logoutText: _.template("Goodbye<% if (name) { %>, <% } %><%= name %>. Hope to see you again soon."),
        welcome_msg: _.template("Welcome <%= name %>!"),
        trophies: "Trophies",
        stars: "Stars",
        answers: "Answers",
        comments: "Comments",
        questions: "Questions",
        statistics: "Statistics"
    };
})();