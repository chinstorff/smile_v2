(function(app) {
    app.ProfileView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            var user = app.getUser();
            if (!this.template) app.ProfileView.prototype.template = this.template || _.template($('#profile-template').html(), {user: user});
        },

        render: function() {
            this.$el.html(this.template);
            logger.info("ProfileView rendered");

            return this;
        },

        events: {
            'submit #profile-form': 'saveUser'
        },

        saveUser: function(ev) {
            ev.preventDefault(); // Don't let this button submit the form
            var userDetails = $(ev.currentTarget).serializeObject();
            
            var user = app.getUser();
            user.save(userDetails, {
                success: function(m, r) {
                    alert('updated user successfully');
                    
                },
                error: function(m, r) {
                    alert('Failed to update the user! ');
                    
                }
            });
            return false;
        },

        remove: function() {
            logger.info("ProfileView removed");
            this.parentRemove();
        }
    })
})(app);