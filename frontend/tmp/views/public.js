(function (app) {

    app.PublicView = app.View.extend({
        events: {
            "click .search-input-clear": 'clearSearchInput',
            "keyup .search-input-text": 'typeSearchInput',
            "focusin .search-input-text": function () {
                $('.input-search').addClass("input-focus");
            },
            "focusout .search-input-text": function () {
                $('.input-search').removeClass("input-focus");
            },
            "change #select-search-order": "orderChange"
        },

        initialize: function () {
            this.generatedCount = 0;
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.PublicView.prototype.template = this.template || _.template($('#public-template').html());
            this.listenTo(this.model, 'change:questions', this.render);
            this.listenTo(this.model, 'change:searchText', this.search);
        },

        model: new app.PublicSearchModel({
            searchQuery: "",
            orderId: 0,
            start: 0,
            range: 10
        }),

        orderChange: function (e) {
            var searchOrder = ($(e.target).val());
            var s = app.lang.get('searchText', searchOrder);
            var ss = app.lang.get('orderBy') + s;
            $(".select-text").html(ss);
        },

        clearSearchInput: function () {
            $('.search-input-text').val('');
            $('.search-input-clear').hide();
        },

        typeSearchInput: function (e) {
            if ($('.search-input-text').val().length > 0) {
                $('.search-input-clear').show();
            } else {
                $('.search-input-clear').hide();
            }
            if (e.keyCode == 13) {
                //this.model.unset('questions', {silent: true});
                var searchText = $('.search-input-text').val();
                //this.model.set('searchText', searchText);
                this.model.fetchQuestions({searchText: searchText});
            }
        },
        search: function(ev) {
            this.model.unset('questions', {silent: true});
            
            
            this.render();
        },
        render: function (options) {
            var questions = this.model.get('questions');
            var avatar_style = app.settings.get('avatar_style');
            if (questions) {
                var graphsToRender = [];
                this.$el.html(this.template({questions: this.model.get('questions'), toRender: graphsToRender, avatar_style: avatar_style, noTransition: app.no_transition}));
                //render graphs
                _.each(graphsToRender, function (el) {
                    var pieChart = new app.utilities.pieChart(el.id,
                        {
                            includeLabels: false,
                            data: [el.val * 360, (1 - el.val) * 360],
                            labels: ["" + (el.val || ""), "" + (1 - el.val || "")],
                            colors: [
                                ["#ff704a", "#FFFFFF"],
                                ["#29b807", "#FFFFFF"]
                            ]
                        }
                    );
                    pieChart.draw();
                    
                });
                //$('.search-input-clear').hide();
                if (app.mobile_agent) {
                    this.initIscroll();
                }
                logger.info("PublicView rendered");
            } else {
                var noTransition = app.no_transition;
                this.$el.html(app.loadingTemplate({msg: "Loading questions...", noTransition: noTransition}));
                this.model.fetchQuestions(options);
            }
            return this;
        },

        remove: function () {
            logger.info("PublicView removed");
            this.parentRemove();
        }

    });
    if (app.mobile_agent) {
        app.PublicView = app.PublicView.extend({
            pullDownAction: function () {
                var that = this;
                //refresh
                /*setTimeout(function () {	// <-- Simulate network congestion, remove setTimeout from production!
                 var el, li, i;
                 el = $('.question-list').get(0);

                 for (i = 0; i < 3; i++) {
                 li = document.createElement('li');
                 $(li).addClass("question-li");
                 li.innerText = 'Generated question ' + (++that.generatedCount);
                 //el.insertBefore(li, el.childNodes[2]);
                 el.appendChild(li, el.childNodes[0]);
                 }

                 that.myScroll.refresh();		// Remember to refresh when contents are loaded (ie: on ajax completion)
                 }, 1000);	// <-- Simulate network congestion, remove setTimeout from production!*/
            },

            pullUpAction: function () {
                var that = this;
                setTimeout(function () {	// <-- Simulate network congestion, remove setTimeout from production!
                    var el, li, i;
                    el = $('.question-list').get(0);

                    for (i = 0; i < 3; i++) {
                        li = document.createElement('li');
                        $(li).addClass("question-li");
                        li.innerText = 'Generated question ' + (++that.generatedCount);
                        el.appendChild(li, el.childNodes[0]);
                    }

                    that.myScroll.refresh();		// Remember to refresh when contents are loaded (ie: on ajax completion)
                }, 1000);	// <-- Simulate network congestion, remove setTimeout from production!
            },

            initIscroll: function () {
                var that = this;
                var pullDownEl = $('.pullDown').get(0),
                    pullUpEl = $('.pullUp').get(0);

                var pullDownOffset = pullDownEl.offsetHeight,
                    pullUpOffset = pullUpEl.offsetHeight;

                this.myScroll = new iScroll($('.iscroll-wrapper').get(0), {
                    useTransition: true,
                    topOffset: pullDownOffset,
                    onRefresh: function () {
                        if ($(pullDownEl).hasClass('loading')) {
                            $(pullDownEl).removeClass('loading');
                            pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
                        } else if ($(pullUpEl).hasClass('loading')) {
                            $(pullUpEl).removeClass('loading');
                            pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
                        }
                    },
                    onScrollMove: function () {
                        if (this.y > 5 && !$(pullDownEl).hasClass('flip')) {
                            $(pullDownEl).addClass('flip');
                            pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Release to refresh...';
                            this.minScrollY = 0;
                        } else if (this.y < 5 && $(pullDownEl).hasClass('flip')) {
                            $(pullDownEl).removeClass('flip');
                            pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
                            this.minScrollY = -pullDownOffset;
                        } else if (this.y < (this.maxScrollY - 5) && !$(pullUpEl).hasClass('flip')) {
                            $(pullUpEl).addClass('flip');
                            pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Release to refresh...';
                            this.maxScrollY = this.maxScrollY;
                        } else if (this.y > (this.maxScrollY + 5) && $(pullUpEl).hasClass('flip')) {
                            $(pullUpEl).removeClass('flip');
                            pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
                            this.maxScrollY = pullUpOffset;
                        }
                    },
                    onScrollEnd: function () {
                        if ($(pullDownEl).hasClass('flip')) {
                            $(pullDownEl).removeClass('flip').addClass('loading');
                            pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Loading...';
                            that.pullDownAction();	// Execute custom function (ajax call?)
                        } else if ($(pullUpEl).hasClass('flip')) {
                            $(pullUpEl).removeClass('flip').addClass('loading');
                            pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Loading...';
                            that.pullUpAction();	// Execute custom function (ajax call?)
                        }
                    },
                    onBeforeScrollStart: function (e) {
                        //fix to alllow selecting input text: https://github.com/cubiq/iscroll/issues/83
                        var target = e.target;
                        while (target.nodeType != 1) target = target.parentNode;

                        if (target.tagName != 'SELECT' && target.tagName != 'INPUT' && target.tagName != 'TEXTAREA')
                            e.preventDefault();
                    }
                });

                document.addEventListener('touchmove', function (e) {
                    e.preventDefault();
                }, false);
            }
        });
    }
})(app);