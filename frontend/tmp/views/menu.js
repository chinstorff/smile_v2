(function (app) {

    app.MenuView = app.View.extend({

        initialize: function () {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.MenuView.prototype.template = this.template || _.template($('#menu-template').html());
        },

        render: function (options) {
            var view_name = this.options.view_name;
            var items = {
                home: {
                    name: "home",
                    title: app.lang.get("home")
                },
                public: {
                    name: "public",
                    title: app.lang.get("public")
                },
                groups: {
                    name: "groups",
                    title: app.lang.get("groups")
                },
                profile: {
                    name: "profile",
                    title: app.lang.get("profile")
                },
                messages: {
                    name: "messages",
                    title: app.lang.get("messages")
                },
                settings: {
                    name: "settings",
                    title: app.lang.get("settings")
                },
                about: {
                    name: "about",
                    title: app.lang.get("about")
                },
                logout: {
                    name: "logout",
                    title: app.lang.get("logOut")
                }
            }
            var colorChange = app.settings.get("menu_color_change");
            var animateTop = (app.settings.get("menu_animate_color") && "-webkit-transition:background-color 300ms linear") || "";
            var animateLi = (app.settings.get("menu_animate_color") && "-webkit-transition:border 500ms linear") || "";
            this.$el.html(this.template({items: items, selected: view_name, menu_color_change: colorChange, animateTop: animateTop, animateLi: animateLi}));
            return this;
        },

        remove: function () {
            logger.info("menu removed");
            this.parentRemove();
        }
    })
})(app);