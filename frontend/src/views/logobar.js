(function (app) {
    app.LogoBarView = app.View.extend({
        initialize: function () {
            app.dispatcher.on("alert", this.showAlert);
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.LogoBarView.prototype.template = this.template || _.template($('#logobar-template').html());
        },

        render: function () {
            logger.info("Rendering LogoBar");
            this.$el.html(this.template());
            return this;
        },

        remove: function () {
            logger.info("LogoBar removed");
            this.parentRemove();
        },

        showAlert: function (alert) {
            var template = _.template($('#alert-template').html());
            $('.alert-container').append(template({
                alert_type: alert.type,
                alert_message: alert.msg
                    .hide().show(400)     }));
        },

        setHeader: function () {

        }
    })
})(app);