(function (app) {
    app.View = Backbone.View.extend({
        fetchError: function (msg, response, retryFunc) {
            var code = response.status;
            console.log("Error code: " + code);
            //default go to login page
            if (code == 401 || code == 403) {
                app.router.navigate("login", {trigger: true, replace: true});
                return;
            }
            msg = msg || "Error reaching the server. ";
            logger.error(msg);
            $('.loading').addClass("error");
            var retryTime = 5;
            var that = this;
            delay(retryTime);
            function delay(retryTime) {
               $('.loading-msg').html(msg + "Retrying in <span class = 'retryNum'>" + retryTime + "</span>.");
                if (retryTime > 0) {
                    that.timer = setTimeout(function () {
                        delay(retryTime - 1);
                    }, 1000);
                } else {
                    retryFunc.call(that);
                }
            }
        },
        parentRemove: function() {
            $('.loading').removeClass("error");
            if (this.timer) clearTimeout(this.timer);
            this.undelegateEvents();
        }
    });
})(app);