(function(app) {
    var menu_width = '220px',
        overlay_opacity = 0.7;

    app.NavbarView = app.View.extend({
        events: {
            'click .navbar-menu': 'menuClick',
            'touchstart .navbar-menu': 'menuDown',
            'touchend .navbar-menu': 'menuUp',
            'touchstart .navbar-settings': 'settingsDown',
            'touchend .navbar-settings': 'settingsUp',
            'touchstart .backbutton': 'backDown',
            'touchend .backbutton': 'backUp'
        },

        initialize: function() {
            app.dispatcher.on("alert", this.showAlert);
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.NavbarView.prototype.template = this.template || _.template($('#navbar-template').html());
        },

        render: function() {
            logger.info("Rendering NavBar");
            var backbutton = false,
                back_href, back_text;
            if (this.options.back_href) {
                backbutton = true,
                back_href = this.options.back_href,
                back_text = this.options.back_text;
            }

            this.$el.html(this.template({backbutton: backbutton, back_href: back_href, back_text: back_text}));
            return this;
        },

        showAlert: function(alert) {
            var template = _.template($('#alert-template').html());
            $('.alert-container').append(template({
                alert_type: alert.type,
                alert_message: alert.msg
            })).hide().show(400);
        },

        remove: function() {
            logger.info('navbar removed');
            this.parentRemove();
        },

        menuClick: function(ev) {
            this.openMenu(ev);
        },

        overlayLick: function(ev) {
            this.closeMenu();
        },

        logoClick: function(ev) {
            this.closeMenu();
        },
        menuDown: function(ev) {
            $('.navbar-menu').addClass('touched');
        },
        menuUp: function(ev) {
            $('.navbar-menu').removeClass('touched');
        },
        settingsDown: function(ev) {
            $('.navbar-settings').addClass('touched');
        },
        settingsUp: function(ev) {
            $('.navbar-settings').removeClass('touched');
        },
        backDown: function(ev) {
            $('.backbutton-left').addClass('touched');
            $('.backbutton-mid').addClass('touched');
            $('.backbutton-right').addClass('touched');
        },
        backUp: function(ev) {
            $('.backbutton-left').removeClass('touched');
            $('.backbutton-mid').removeClass('touched');
            $('.backbutton-right').removeClass('touched');
        },
        openMenu: function(ev) {
            var style = app.settings.get("menu_animation");
            //build menu
            //get current view name
            var view_name = app.router.mainView.name;
            var menu = new app.MenuView({view_name: view_name});
            menu.setElement($('#menu-container')).render();
            $('#menu-overlay').addClass("menu-overlay");
            var menuHeight = $('#menu-container').height();
            $('#viewport').height(menuHeight);
            if (style == "push") {
                $("#app-container").addClass("pushOpen");
                $(".iscroll-wrapper").addClass("revealOpen");
            } else if (style == "overlap") {
                $("#menu-container").addClass("overlapOpen");
            } else if (style == "reveal") {
                $("#app-container").addClass("revealOpen");
                $(".iscroll-wrapper").addClass("revealOpen");
                $("#menu-overlay").addClass("revealOpen");
            }
            if (app.settings.get("menu_fadeout")) {
                document.getElementById("menu-overlay").style.opacity = overlay_opacity;
            }
            //create close listeners
            var that = this;
            $('#menu-overlay').bind('click', function() {
                that.closeMenu(style);
            });
            $('.menu-logo').bind('click', function() {
                that.closeMenu(style);
            });
            $('.menu-li').bind('click', function(e) {
                var el = e.target;
                if ($(e.target).hasClass("menu-li-img")) {
                    el = $(el).parent()[0];
                }
                that.buttonClick(el, style);
            });
        },

        closeMenu: function(style) {
            //unbind events
            $('#menu-overlay').unbind('click');
            $('.menu-logo').unbind('click');
            $('.menu-li').unbind('click');
            $('#menu-overlay').removeClass("menu-overlay");
            $('#viewport').height('auto');
            if (style == "push") {
                $("#app-container").removeClass("pushOpen");
                $(".iscroll-wrapper").removeClass("revealOpen");
                document.getElementById("menu-overlay").style.opacity = 0;
            } else if (style == "overlap") {
                $("#menu-container").removeClass("overlapOpen");
                document.getElementById("menu-overlay").style.opacity = 0;
            } else if (style == "reveal") {
                $("#app-container").removeClass("revealOpen");
                $("#menu-overlay").removeClass("revealOpen");
                $(".iscroll-wrapper").removeClass("revealOpen");
                document.getElementById("menu-overlay").style.opacity = 0;;
            }
        },

        buttonClick: function(el, style) {
            var name = el.className.replace("menu-li","").replace(" ","");
            var route;
            switch (name) {
                case "home":
                    route = "home";
                    break;
                case "public":
                    route = "public";
                    break;
                case "groups":
                    route = "groups";
                    break;
                case "messages":
                    route = "messages";
                    break;
                case "profile":
                    route = "profile";
                    break;
                case "about":
                    route = "about";
                    break;
                case "settings":
                    route = "settings";
                    break;
                case "logout":
                    route = "logout";
                    break;
                default:
                    break;
            }
            if (route) {
                //set to new color if changed
                if (app.settings.get('menu_color_change') && app.settings.get('menu_instant_color_change')) {
                    $('.menu-top').addClass(name).removeClass(app.router.content.name);
                }
                //trigger calls route function if true, replace doesn't update browser's history if true
                app.router.navigate(route, {trigger: true});
            }
            this.closeMenu(style);
        },

        setHeader: function(options) {
            if (options.text) {
                $('.navbar-title').html(options.text);
            }
        }
    })
})(app);