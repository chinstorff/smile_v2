(function(app) {
    app.GroupsView = app.View.extend({
        initialize: function() {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.GroupsView.prototype.template = this.template || _.template($('#about-template').html());
        },

        render: function() {
            this.$el.html(this.template());
            logger.info("GroupsView rendered");

            return this;
        },

        remove: function() {
            logger.info("GroupsView removed");
            this.parentRemove();
        }
    })
})(app);